const express = require("express");
const Router = express.Router();

const secureRoutes = require("../middlewares/secureRoutes");
const checkRole = require("../middlewares/checkRole");

const auth = require("./auth");
const user = require("./user");
const truck = require("./truck");
const load = require("./load");
//

Router.use("/auth", auth);
Router.use("/users", secureRoutes, user);
Router.use("/trucks", secureRoutes, checkRole("DRIVER"), truck);
Router.use("/loads", secureRoutes, load);

module.exports = Router;
