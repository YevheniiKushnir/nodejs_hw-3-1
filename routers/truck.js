const express = require("express");
const Router = express.Router();

const {
  createTruck,
  getTrucks,
  getTruckById,
  deleteTruckById,
  changeTruckById,
  assignTruckById,
} = require("../services/truck");

Router.get("/", getTrucks);
Router.get("/:id", getTruckById);

Router.post("/", createTruck);
Router.post("/:id/assign", assignTruckById);
Router.put("/:id", changeTruckById);

Router.delete("/:id", deleteTruckById);

module.exports = Router;
