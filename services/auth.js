const bcrypt = require("bcryptjs");
const Joi = require("joi");
const jwt = require("jsonwebtoken");

const { schemaJoiPerson, Person } = require("../models/Person");

const getDate = require("../helpers/getDate");

const schemaJoiLogin = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
    .error(new Error("Email must be valid")),
  password: Joi.string()
    .pattern(new RegExp("^[a-zA-Z0-9]{3,30}$"))
    .error(new Error("Password must be valid")),
});

const register = async (req, res, next) => {
  try {
    const { email, password, role } = req.body;

    await schemaJoiPerson.validateAsync({ email, password, role });

    const person = new Person({
      created_date: getDate(),
      email,
      password: await bcrypt.hash(password, 10),
      role,
    });

    person
      .save()
      .then((saved) =>
        res.status(200).json({
          message: "Profile created successfully",
        })
      )
      .catch((err) => {
        res.status(400).json({ message: "User exist" });
      });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const login = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    await schemaJoiLogin.validateAsync({ email, password });

    const user = await Person.findOne({ email });
    if (!user) throw Error("User not exist");

    const comparePasswords = await bcrypt.compare(String(password), String(user.password));

    if (user && comparePasswords) {
      const payload = { email: user.email, userId: user._id, role: user.role };

      const jwtToken = jwt.sign(payload, process.env.JWT_SECRET_KEY);

      return res.status(200).json({
        jwt_token: jwtToken,
      });
    } else throw Error("Not authorized");
  } catch (error) {
    res.status(400).json({ message: error.message || "Bad request" });
  }
};

const resetPassword = (req, res, next) => {
  /* reset password logic */
  res.status(500).json({ message: "not exist" });
};

module.exports = {
  register,
  login,
  resetPassword,
};
