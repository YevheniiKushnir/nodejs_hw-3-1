const mongoose = require("mongoose");

const Joi = require("joi");

const schemaJoiPerson = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
    .required()
    .error(new Error("Email must be valid")),
  password: Joi.string()
    .pattern(new RegExp("^[a-zA-Z0-9]{3,30}$"))
    .required()
    .error(new Error("Password must be valid")),
  role: Joi.string().alphanum().required().error(new Error("Empty role")),
});

const person = new mongoose.Schema({
  created_date: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
});

const Person = mongoose.model("Users", person);

module.exports = { Person, schemaJoiPerson };
