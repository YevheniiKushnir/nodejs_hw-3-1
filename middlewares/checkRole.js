const checkRole = (role) => {
  return (req, res, next) => {
    if (role !== req.user.role) return res.status(400).json({ message: "Missed role" });
    next();
  };
};

module.exports = checkRole;
